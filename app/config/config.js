// Setting application configuration
const configSettings = {
    'local': {
        'host': 'local',
        'appName': 'WebJet Project',
        'http': {
            'address': 'localhost',
            'port': '8089'
        },
        'movieLand': {
            'url': 'http://movieland.com/api'
        },
        'filmWorld': {
            'url': 'http://filmworld.com/api'
        }
    }
}

const config = configSettings[process.env.APP_ENV || 'local'];
module.exports = config;