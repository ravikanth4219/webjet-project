// This builds the swagger object from YAML to JSON
const fs = require('fs')
const jsYaml = require('js-yaml');


buildSwagger = () => {
    let spec = fs.readFileSync('./app/api/swagger.yaml', 'utf-8');
    let swaggerDoc = jsYaml.safeLoad(spec);
    fs.writeFile('./app/api/swagger.json', JSON.stringify(swaggerDoc, null, 2), (err) => {
        if (err) {
            console.log(`Error ocurred while writing swagger JSON file`);
        }
    })
    return swaggerDoc;
    console.log(`Swagger is generated for application`)
}

module.exports = buildSwagger()
