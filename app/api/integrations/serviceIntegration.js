// This integrates webjet application to MovieLand and FilmWorld web-services
const request = require('request')
const config = require('../../config/config')
require('../mocks/webjet') // This includes http request mocks

const service = {
    // This will invoke MovieLand service specific integration calls
    movieLand: {
        findALL: function (req, res, next) {       
            var options = {
                url: `${config.movieLand.url}/movies`,
                method: 'GET',
                headers: {
                    'User-Agent': 'request'
                }
            };               
            function callback(error, response, body) { 
                if (!error && response.statusCode == 200) {
                    var info = JSON.parse(body);
                    next(info)
                } else {
                    next(error)
                }
            }               
            request(options, callback);
        },
        findById: function (req, res, next) {
            var movieID = req.swagger.params.movieId.value;       
            var options = {
                url: `${config.movieLand.url}/movie/${movieID}`,
                method: 'GET',
                headers: {
                  'User-Agent': 'request'
                }
            };                
            function callback(error, response, body) {
                if (!error && response.statusCode == 200) {
                    var info = JSON.parse(body);
                    next(info)
                } else {
                    next(error)
                }
            }               
            request(options, callback);
        }
    },
    // This will invoke FilmWorld service specific integration calls
    filmWorld: {
        findALL: function (req, res, next) {       
            var options = {
                url: `${config.filmWorld.url}/movies`,
                method: 'GET',
                headers: {
                    'User-Agent': 'request'
                }
            };               
            function callback(error, response, body) { 
                if (!error && response.statusCode == 200) {
                    var info = JSON.parse(body);
                    next(info)
                } else {
                    next(error)
                }
            }               
            request(options, callback);
        },
        findById: function (req, res, next) {
            var movieID = req.swagger.params.movieId.value;       
            var options = {
                url: `${config.filmWorld.url}/movie/${movieID}`,
                method: 'GET',
                headers: {
                  'User-Agent': 'request'
                }
            };                
            function callback(error, response, body) {
                if (!error && response.statusCode == 200) {
                    var info = JSON.parse(body);
                    next(info)
                } else {
                    next(error)
                }
            }               
            request(options, callback);
        }
    }
}

module.exports = service;
