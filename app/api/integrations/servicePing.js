// This integrates webjet application to MovieLand and FilmWorld web-services to ping test
const request = require('request')
const config = require('../../config/config')
require('../mocks/webjet') // This includes http request mocks

const service = {
    // This will invoke MovieLand service ping call
    movieLand: {
        ping: function() {
            return new Promise((resolve, reject) => {
                console.log('Received Ping check for Movie Land service.')
                var options = {
                    url: `${config.movieLand.url}/ping`,
                    method: 'GET',
                    headers: {
                        'User-Agent': 'request'
                    }
                };                
                function callback(error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var info = JSON.parse(body);
                        resolve(info)
                    } else {
                        reject(error)
                    }
                }               
                request(options, callback);
            })
        }            
    },
    // This will invoke FilmWorld service ping call
    filmWorld: {
        ping: function() {
        return new Promise((resolve, reject) => {
            console.log('Received Ping check for Film World service.')
                var options = {
                    url: `${config.filmWorld.url}/ping`,
                    method: 'GET',
                    headers: {
                      'User-Agent': 'request'
                    }
                };                
                function callback(error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var info = JSON.parse(body);
                        resolve(info)
                    } else {
                        reject(error)
                    }
                }               
                request(options, callback);
            }) 
        }           
        }
}



module.exports = service;