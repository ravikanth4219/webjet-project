// This is controller for the application
const service = require('../integrations/serviceIntegration') // adding service integrations
const serviceCheck = require('../integrations/servicePing')

// Controller action to search a movie by ID
module.exports.findMovieById = function findMovieById (req, res, next) {
  // This promise pings to check available service and picks whichs responds quicker
  // Assuming the service provides enpoints for healthcheck
    Promise.race([serviceCheck.movieLand.ping(), serviceCheck.filmWorld.ping()])
        .then(function(result) {
        console.log(`Received ping response from: ${result.service}`)
        // Here it calls the responded service
        service[result.service].findById(req,res, function(result) {
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(result));
        })
      })
      .catch((err) => {
        console.log(`Error retrieving requested data: ${err}`)
      })
};

// Controller action to search all available movies
module.exports.findAllMovies = function findAllMovies (req, res, next) {
  // This promise pings to check available service and picks whichs responds quicker
  // Assuming the service provides enpoints for healthcheck
    Promise.race([serviceCheck.movieLand.ping(), serviceCheck.filmWorld.ping()])
    .then(function(result) {
    console.log(`Received ping response from: ${result.service}`)
    // Here it calls the responded service
    service[result.service].findALL(req,res, function(result) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(result));
    })
  })
  .catch((err) => {
    console.log(`Error retrieving requested data: ${err}`)
  })
};