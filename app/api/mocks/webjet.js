// Here we initialize http mocks for the service
const nock = require('nock');
const fs = require('fs');
const data = JSON.parse(fs.readFileSync('./app/api/mocks/movieData.json', 'utf-8'));
const movieList = data.list;
const getId = (id, cb) => {
    movieList.forEach(element => {
        if (id === element.id) {
            cb(null, [200, element])
        }
    });
}

// Functions mlPingTime and fwPingTime are used to generate random response times
const mlPingTime = (cb) => {
    var x = Math.floor((Math.random() * 10) + 1);
    setTimeout(function(){ cb(null, [200, {service:"movieLand"}]); }, x*100);
}

const fwPingTime = (cb) => {
    var x = Math.floor((Math.random() * 10) + 1);
    setTimeout(function(){ cb(null, [200, {service:"filmWorld"}]); }, x*100);
}

// MovieLand service nock initialization
nock("http://movieland.com/api/movies")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        cb(null, [200, movieList])
        });
nock("http://movieland.com/api/movie/1")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        getId(uri.substr(11,12), cb)
    });
nock("http://movieland.com/api/movie/2")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        getId(uri.substr(11,12), cb)
    });
nock("http://movieland.com/api/movie/3")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        getId(uri.substr(11,12), cb)
    });
nock("http://movieland.com/api/movie/4")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        getId(uri.substr(11,12), cb)
    });
nock("http://movieland.com/api/movie/5")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        getId(uri.substr(11,12), cb)
    });    
nock("http://movieland.com/api/ping")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        mlPingTime(cb)
    });                                    

// FilmWorld service nock initialization
nock("http://filmworld.com/api/movies")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        cb(null, [200, movieList])
        });

nock("http://filmworld.com/api/movie/1")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        getId(uri.substr(11,12), cb)
    });
nock("http://filmworld.com/api/movie/2")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        getId(uri.substr(11,12), cb)
    });
nock("http://filmworld.com/api/movie/3")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        getId(uri.substr(11,12), cb)
    });
nock("http://filmworld.com/api/movie/4")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        getId(uri.substr(11,12), cb)
    });
nock("http://filmworld.com/api/movie/5")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        getId(uri.substr(11,12), cb)
    });    
nock("http://filmworld.com/api/ping")
    .persist()
    .get("")
    .reply(function(uri, requestBody, cb){
        fwPingTime(cb)
    });                                     