'use strict';
const app = require('connect')();
const http = require('http');
const swaggerTools = require('swagger-tools');
const config = require('./app/config/config')
var swaggerDoc = require('./app/api/index')

if (swaggerDoc && swaggerDoc !== undefined) {
// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {
  app.use(middleware.swaggerMetadata());

  // Validate Swagger requests
  app.use(middleware.swaggerValidator());

  app.use(middleware.swaggerSecurity({
    ApiKeyAuth: function(req, authOrSecDef, scopesOrApiKey, callback) {
      if(req.swagger && req.swagger.params['x-app-token'] && req.swagger.params['x-app-token'] !== undefined) {
        console.log(`Request successfully authenticated`)
        callback(null)
      } else {
        console.log(`Token required`)
        callback({msg:'Need valid'})
      }
    }
  }))

  // Route validated requests to appropriate controller
  app.use(middleware.swaggerRouter({controllers: './app/api/controllers'}));

  // Serve the Swagger documents and Swagger UI
  app.use(middleware.swaggerUi());

  // Start the server
  http.createServer(app).listen(config.http.port, function () {
    console.log(`Your ${config.appName} server is listening at (http://${config.http.address}:${config.http.port})`);
  });
});
}
