1) Assumed MovieLand and FilmWorld as webservices integrated to Webjet application
2) Used Nock to mock the http response for above service requests
3) Assumed third-party services have ping as healthcheck
4) Commands to follow:
    a) npm install (To install dependencies)
    b) npm start (To start the service)
    c) npm test (To run tests)
5) For API definitions and swagger, visit below links in browser after server is started:
    a) http://localhost:8089/docs/

    b) http://localhost:8089/api-docs

6) Used postman application to test the API

7) Endpoint to search all movies (after server started):
    http://localhost:8089/api/movies

    Token is required in the header for every request as key-pair value 
    (x-app-token:sjd1HfkjU83ksdsm3802k)
8) Endpoint to get a particular movie using id (after server started): 

    http://localhost:8089/api/movie/1 (Here 1 is id passed in path, you can use 1-5 as id values to test)

    Token is required in the header for every request as key-pair value 
    (x-app-token:sjd1HfkjU83ksdsm3802k)



