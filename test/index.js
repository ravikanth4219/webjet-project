// Testing application
const test = require('tape');
const request = require('supertest')
const app = require('../index')
const server = request.agent("http://localhost:8089");

// Test for a specific movie search by ID
test('Find movie by ID', function (t) {
    server
    .get('/api/movie/1')
    .set('x-app-token', 'sjd1HfkjU83ksdsm3802k')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
        console.log(res.body)
        if(err) {
            t.error(err,"Unable to get data")
        }
        t.end();
    })
});

// Test for all movie search functionlaity
test('Find all movies', function (t) {
    server
    .get('/api/movies')
    .set('x-app-token', 'sjd1HfkjU83ksdsm3802k')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
        console.log(res.body)
        if(err) {
            t.error(err,"Unable to get data")
        }
        t.end();
    })
});

test.onFinish(function() {
    process.exit(0);
})